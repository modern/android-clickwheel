Android Clickwheel | Колёсико прокрутки
=============
Это зеркало моего проекта: [https://github.com/VadimDev/android-seekbar-like-ipod-clickwheel](https://github.com/VadimDev/android-seekbar-like-ipod-clickwheel)

![Sample Image](https://github.com/VadimDev/android-seekbar-like-ipod-clickwheel/blob/master/ru.clickwheel.TestActivity/res/raw/sample_image.png?raw=true
 "An example implementation")
![Sample Image 2](https://github.com/VadimDev/android-seekbar-like-ipod-clickwheel/blob/master/ru.clickwheel.TestActivity/res/raw/sample_image_1.png?raw=true "Another example implementation")

**Vadim Petrov - 2012**